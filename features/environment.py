from selenium import webdriver
import pytest
import time
import json
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

def before_all(context):
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--no-sandbox')
    #chrome_options.add_argument('--window-size=1420,1080')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--disable-gpu')
    context.browser = webdriver.Chrome(chrome_options=chrome_options)
    # context.browser.set_window_size(1920, 1080)
    context.browser.implicitly_wait(1)
    context.server_url = 'https://wizebin-dashboard-staging.herokuapp.com/'

def after_all(context):
    # Explicitly quits the browser, otherwise it won't once tests are done
    context.browser.quit()
