from behave import given, when, then
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


@given(u'acesso a pagina inicial do waste')
def step_impl(context):
   context.browser.get(context.server_url+"/login")

@given(u'digitei um email errado')
def step_impl(context):
    context.browser.find_element(By.ID, "email").send_keys("wize@wuize.com")
    
@given(u'digitei uma senha errada')
def step_impl(context):
    context.browser.find_element(By.ID, "password").send_keys("1234")

@when(u'clico em login')
def step_impl(context):
    context.browser.find_element(By.CSS_SELECTOR, ".login-button-inside").click()
  

@then(u'devo estar na pagina de login')
def step_impl(context):
    resultado_desejado = "https://wizesystems-dashboard-staging.herokuapp.com/login?product=waste"
    assert ( resultado_desejado == context.browser.current_url)
  